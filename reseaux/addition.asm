# 3e programme ; doubler un nombres
.text    
  main:
        # demande d'entrer un nombre
	li $v0, 4               
	la $a0, lecture
	syscall	
	# Lit le nombre et le stocke dans $s0
	li $v0, 5  # le code 5 correspond à la lecture d'une donnée
	syscall
	move $s0, $v0
	
	li $v0, 4               
	la $a0, lecture
	syscall	
	# Lit le nombre et le stocke dans $s0
	li $v0, 5  # le code 5 correspond à la lecture d'une donnée
	syscall
	move $s1, $v0	
	# additionne
	add $s0, $s1, $s0
	# Affiche le texte d'introduction du résultat
	li $v0, 4
	la $a0, affichage
	syscall
	# Affiche le nombre obtenu
	li $v0, 1
	move $a0, $s0
	syscall
	# termine le programme
	li $v0, 10  # 10 code la terminaison du programme
	syscall      # exécute l'action codée dans $v0 : fin du programme
	
.data    
  lecture: .asciiz "Entrez un nombre : \n"
  affichage: .asciiz "\nLa somme des nombre entrés est : "  
