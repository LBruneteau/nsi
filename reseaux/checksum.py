def correct_checksum(paquet):
    check = sum(paquet[:5] + paquet[6:])
    while check > 0xffff:
        string = hex(check)[2:]
        check = int(string[1:], 16)
        check += int(string[0], 16)
    binaire = bin(check)
    complement = ''.join(['0' if i == '1' else '1' for i in binaire[2:]])
    return int(complement, 2) == paquet[5]

paquet = (0x4500, 0x009b, 0x5bff, 0x4000, 0x3406, 0x2452, 0xc0a8, 0x0019, 0x50f3, 0xb457)
print(correct_checksum(paquet))