from bottle import route, post, install, template, request, run
from bottle_sqlite import SQLitePlugin

install(SQLitePlugin(dbfile='./eleves.db'))

@route('/')
def index():
    return template('views/index.tpl')

@post('/creation')
def creation(db):
    db.execute("""
    CREATE TABLE IF NOT EXISTS eleves (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        prenom TEXT,
        nom TEXT,
        nais TEXT,
        adr TEXT,
        ville TEXT,
        cp TEXT,
        email TEXT,
        UNIQUE(nom, prenom, email)
        )
        """
    )
    with open('./baseDpts.sql', 'r') as dpt:
        db.executescript(dpt.read())
    return template('views/index.tpl')

@route('/entreNouveau')
def entreNouveau():
    return template('views/eleve.tpl')

@post('/ajouteEntree')
def ajouteEntree(db):
    vals = tuple([request.forms[cle] for cle in ('prenom', 'nom', 'nais', 'adr', 'ville', 'cp', 'email')])
    db.execute('INSERT INTO eleves (prenom, nom, nais, adr, ville, cp, email) VALUES (?,?,?,?,?,?,?)', vals)
    return template('views/index.tpl')

@route('/liste')
def liste(db):
    return template('views/liste.tpl', lignes=db.execute("SELECT * FROM eleves").fetchall())

@route('/recherche')
def recherche():
    return template('views/recherche.tpl')

@post('/requete')
def requete(db):
    champ = request.forms['champ']
    lignes = db.execute(f"SELECT * FROM eleves WHERE {champ} LIKE ?", (f"%{request.forms['motcle']}%",))
    return template('views/requete.tpl', res=lignes.fetchall())

if __name__ == '__main__':
    run(reloader = True, debug = True, host='127.0.0.1', port=7000)
