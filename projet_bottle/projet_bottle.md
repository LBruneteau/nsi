# Projet Bottle

## Préliminaires : construction d'une base de travail

### Construire une base de données

On voudrait inscrire des élèves en ligne en entrant les renseignements suivants :

```mermaid
classDiagram
    class Eleves{
        Text prenom
        Text nom
        Text nais
        Text adr
        Text ville
        Text email
    }
```

Cette relation a l'inconvénient de ne pas avoir de clé primaire. On ajoutera donc  un identifiant numérique unique sous la forme d'un entier.
Pour ne pas à devoir manipuler directement cet identifiant, on laissera `SQLite` choisir et mettre à jour l'identifiant grâce à `AUTOINCREMENT`. Il suffira de créer la colonne avec l'argument :

```sql
id INTEGER PRIMARY KEY AUTOINCREMENT
```

Donner alors la commande SQLite permettant de créer la table `eleves` selon le schéma précédemment introduit.

```sql
CREATE TABLE "Eleves" (
"id"    INTEGER NOT NULL UNIQUE,
    "prenom"    TEXT NOT NULL,
    "nom"   TEXT NOT NULL,
    "nais"  TEXT,
    "ville" TEXT,
    "email" TEXT,
    PRIMARY KEY("id")
);
```

### Rechercher dans une table selon un motif

On voudrait effectuer des recherches dans la table élèves selon certains motifs (noms qui commencent par "abs", habitants de la Loire-Atlantique, élèves nés en 2003, etc.).

On peut pour cela utiliser `WHERE` et même `WHERE...LIKE...` avec des *wildcards* (quantificateurs). Les plus utiles sont :

* `%` qui représente 0, 1 ou plusieurs caractères quelconques;
* `_` qui représente un et un seul caractère quelconque.

Par exemple `%s` permettra d'avoir toutes les chaînes qui finissent par *s*, `ro_e` permettra d'avoir *robe*, *rose*,...

La syntaxe est :

```sql
SELECT
    liste_de_colonnes
FROM
    nom_de_la_table
WHERE
    colonne LIKE motif;
```

On pourra utiliser la fonction `substr(chaine, deb, fin)` qui extraît une sous-chaîne de `chaine` du caractère numéro `deb` jusqu'au caractère numéro `fin', sachant que le premier caractère porte le numéro 1.

On pourra enfin utiliser `IN...(....)` pour vérifier qu'un champ est élément d'une liste ou d'une sous-requête:

```sql
expression [NOT] IN (liste_de_valeurs|sous_requete);
```

1. Définir une requête qui permet d'obtenir tous les élèves nés en 2002.
2. Définir une requête qui permet d'obtenir tous les élèves ayant un compte e-mail chez l'opérateur sql (du type `pseudo@sql.fr`).
3. Définir une requête qui permet d'obtenir les élèves dont l'adresse e-mail commence par la première lettre de leur prénom.
4. Définir une requête qui permet d'obtenir tous les élèves habitant en Bretagne.

#### Réponses 1

1.

```sql
SELECT * FROM Eleves WHERE nais LIKE "2002%";
```

2.

```sql
SELECT * FROM Eleves WHERE email LIKE "%sql.fr";
```

3.

```sql
SELECT * FROM Eleves WHERE SUBSTR(prenom, 1, 1) = SUBSTR(email, 1, 1);
```

4.

```sql
SELECT * FROM Eleves WHERE adr LIKE "%Bretagne%";
```

### Effectuer des jointures

Un élève doit passer des examens dans la capitale régionale dont dépend son domicile. Mais le ministère ne dispose que de vieilles tables qui correspondaient aux anciennes régions puis de nouvelles tables ajoutées pour s'adapter au nouveau découpage. Voici leur structure:

```mermaid
classDiagram
    class Departements{
        Text num_departement
        Text num_old_region
        Text nom_departement
    }
    class Old_regions{
        Text num_old_region
        Text nom_old_region
    }
    class New_regions{
        Text num_old_region
        Text nom_new_region
        Text num_new_region
    }
    class Capitales{
        Text num_new_region
        Text ville
    }
```

Ainsi

* une ligne de `Departements` sera : `('01', '22', 'Ain')`
* une ligne de `Old_regions` sera : `('14', 'Limousin')`
* une ligne de New_regions` sera : `('14', 'Nouvelle-Aquitaine', '2')`
* une ligne de `Capitales` sera : `('13', 'Nantes')`

1. Quelles pourraient être les clés primaires et éventuellement étrangères de ces tables ?
2. Simplifier ces tables pour qu'elles ne dépendent plus des anciennes régions. Donner les requêtes `SQL` nécessaires.
3. Récuperer le fichier initial `dpts.sql` et effectuer les changements proposés. Enregistrer ensuite les deux  tables restantes dans un fichier SQL nommé `baseDpts.sql` dont nous aurons besoin dans l'exercice **Gérer une base de données SQLite avec Python**.

#### Réponses 2

1. Les clés primaires pourraient être les attributs `num_nomdelatable`et les clés etrangères `num_nomautretable`

2. Créer l'attribut `num_new_region` dans la table `Departements`. Supprimer les attributs `num_old_region` dans toutes les tables et la table `Old_regions`.

```sql
ALTER TABLE Departements ADD num_new_region CHAR(255);
ALTER TABLE Departements DROP COLUMN num_old_region;
ALTER TABLE New_regions DROP COLUMN num_old_region;
DROP TABLE Old_regions;
```