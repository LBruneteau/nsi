# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# 
# ## Listes chaînées

# %%
from typing import Generic, TypeVar, Optional, Union, Callable
from maillon import Maillon


# %%
T = TypeVar("T")


# %%
class ListeC(Generic[T]):

    def __init__(self, *args: T) -> None:
        self._tete: Optional[Maillon[T]] = None
        for arg in reversed(args):
            self.insere_tete(arg)

    @property
    def vide(self) -> bool:
        return self._tete is None

    @property
    def queue(self) -> 'ListeC[T]':
        qt = ListeC()
        if not self._tete is None:
            qt._tete = self._tete.suiv 
        return qt

    def insere_tete(self, val: T) -> None:
        if self._tete is None:
            self._tete = Maillon(val)
        else:
            t = Maillon(val)
            t.suiv = self._tete
            self._tete = t
            
    def __repr__(self) -> str:
        if self.vide:
            return "Vide"
        if self.queue.vide:
            return f"Tete : {self._tete} --> Queue : Vide"
        return f"Tete : {self._tete.val} --> Queue : {self.queue._tete.val} --> ?"

    def __len__(self) -> int:
        if self.vide:
            return 0
        else:
            return 1 + self.queue.__len__()

    def __iter__(self):
        i: Maillon[T] = self._tete
        while i is not None:
            yield i.val
            i = i.suiv

    def __getitem__(self, index: int, valeur: bool = True) -> Union[T, Maillon[T]]:
        l: int = len(self)
        if index >= l or index < -l:
            raise IndexError
        i: Maillon[T] = self._tete
        for _ in range(index if index >= 0 else index + l):
            i = i.suiv
        return i.val if valeur else i

    def insert(self, index: int, element: T) -> None:
        if not index:
            self.insere_tete(element)
        else:
            l: int = len(self)
            if index >= l or index < -l:
                raise IndexError
            if index < 0:
                index += l
            i: Maillon[T] = self._tete
            for _ in range(index - 1):
                i = i.suiv
            m: Maillon[T] = Maillon(element)
            m.suiv = i.suiv
            i.suiv = m

    def append(self, element: T) -> None:
        i: Maillon[T] = self.__getitem__(-1, False)
        i.suiv = Maillon(element)

    def map(self, formule: Callable[[T], T]) -> 'ListeC[T]':
        liste:'ListeC[T]' = ListeC()
        for i in range(1, len(self) + 1):
            liste.insere_tete(formule(self[-i]))
        return liste

    def reverse(self) -> 'ListeC[T]':
        liste = ListeC()
        for i in self:
            liste.insere_tete(i)
        return liste