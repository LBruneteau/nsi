from typing import Callable, TypeVar
from listechainee import ListeC

T = TypeVar('T')

def plusplus(*args: ListeC[T]) -> ListeC[T]:
    '''Ajoute deux listes chaînées
    '''
    res: ListeC[T] = ListeC()
    for liste in reversed(args):
        for i in reversed(liste):
            res.insere_tete(i)
    return res

