from typing import *

T = TypeVar('T')

class Maillon(Generic[T]):

    def __init__(self, val: Optional[T] = None) -> None:
        self._val: Optional[T] = val
        self._suiv: Optional['Maillon[T]'] = None

    @property
    def val(self) -> T:
        return self._val

    @property
    def suiv(self) -> Optional['Maillon[T]']:
        return self._suiv

    @suiv.setter
    def suiv(self, m: Optional['Maillon[T]']) -> None:
        if m is None or isinstance(m._val, type(self._val)):
            self._suiv = m
        else:
            raise TypeError  

    def __repr__(self) -> str:
        return f"[{self._val}]-->{None if self._suiv is None else self._suiv._val}"
