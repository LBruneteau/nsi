'''
Chiffrement de Vigenere
'''
ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"


def lettres(phrase: str) -> str:
    '''renvoie la phrase en majuscule et en ayant supprimé tous les espace'''
    phrase = phrase.replace(' ', '') # On supprime les espaces en les remplaçant par des chaînes de caractères vides } Louis
    return phrase.upper() # Mettre la phrase en majuscule et retourner } Louis


def decaler(lettre: str, decalage: int) -> str:
    '''Renvoie la lettre obtenue par un décalage circulaire de `decalage` rangs de la lettre `lettre` dans l'alphabet
    Si le "Z" est atteint on retourne à "A"
    Parametres:
        lettre : Une lettre de l'alphabet en majuscule
        decalage : le nombre de position dont la lettre doit être décalée, entre 0 et 25
    >>> decaler("A", 2)
    "C"
    >>> decaler("Y", 4)
    "C"
    '''
    return chr(((mesurer_decalage(lettre) + decalage) % 26) + ord("A"))


def mesurer_decalage(lettre: str) -> int:
    '''
    calcule le décalage d'une lettre majuscule par rapport à 'A'
    '''
    return ord(lettre) - ord("A")


def vigenere(message: str, cle: str, mode: str) -> str:
    '''
    chiffre ou dechiffre un message selon la méthode de Vigenere
    '''
    # Si le mode n'est pas chiffrer ou déchiffrer on lance une erreur } Louis
    assert mode in ('chiffrer', 'dechiffrer'), 'Le mode fourni est invalide !'
    # Dans la version d'origine, la variable `signe` restait inutilisée. } Louis
    # Maintenant elle sert à multiplier le décalage par -1 si on déchiffre } Louis
    signe = 1 if mode == "chiffrer" else -1 
    message = lettres(message)
    cle = lettres(cle)
    message_traduit = ''
    for i, lettre in enumerate(message):
        message_traduit += decaler(lettre,
                               signe * mesurer_decalage(cle[i % len(cle)])) # Différence : "signe * mesurer_decalage" } Louis
    # Il faut ajouter le décalage pour chiffrer et le soustraire pour déchiffrer } Louis
    # Multiplier le décalage par -1 quand on doit déchiffrer fait la même chose } Louis
    # De cette façcon le signe est positif quand on chiffre et négatif quand on déchiffre } Louis 
    # Plus besoin de copier coller le code dans des blocs if / else similaires } Louis
    return message_traduit


def tester():
    '''teste les fonctions'''
    assert lettres(" bonjour marcel ") == "BONJOURMARCEL"
    assert decaler('A', 0) == 'A'
    assert decaler('B', 3) == 'E'
    assert decaler('Z', 2) == 'B'
    for lettre in ALPHABET:
        print(decaler(lettre, 2), end='')
    print()
    msg = "bonjour la nsi"
    msg_chiffre = vigenere(msg, "qkzk", "chiffrer")
    print(msg_chiffre)
    msg_dechiffre = vigenere(msg_chiffre, "qkzk", "dechiffrer")
    assert msg_dechiffre == "BONJOURLANSI"


if __name__ == "__main__":
    tester()
