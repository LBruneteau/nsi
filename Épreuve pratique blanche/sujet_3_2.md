---
title: "NSI - Terminale - Epreuve pratique"
subtitle: "Sujet 3 - Exercice 2"
theme: metropolis
geometry: margin=1.5cm

---

# Le chiffrement de Vigenère

Le chiffrement de Vigenère est une variante du chiffrement de César.
Il utilise aussi un décalage des lettres mais, au lieu d'utiliser une même
clé pour chiffrer chaque lettre, on utilise une clé variable.

La clé est ici une phrase qui permet de chiffrer et de dechiffrer. C'est
donc un chiffrement symétrique.

Le principe est le suivant :

Pour chiffrer "NSI" avec la clé "BCD" on regarde, pour chaque lettre du message
la lettre qui lui correspond dans la clé.
Cette lettre est alors associée à son décalage depuis "A"

Ainsi, pour chiffrer "N" avec "B" on compte un décalage de 1 entre "B" et "A"
et on décale "N" de un en "O".

De même :

* "S" est chiffré avec "C" (décalage de 2) en "U" 
* "I" est chiffré avec "D" (décalage de 3) en "L".

Le mot "NSI" est chiffré en "OUL" avec la clé "BCD".

Ce qui rend ce chiffrement intéressant est qu'il a longtemps resisté aux
assauts des cryptanalystes et qu'il a fallu employer des techniques mathématiques
plus modernes pour en venir à bout.

Lorsque le mot est plus long que la clé, on la reprend depuis le début.

Pour déchiffrer on utilise la même méthode sauf qu'on compte le décalage
contraire, depuis la lettre de la clé vers le "A".

Afin de simplifier on ne s'intéressera qu'à des messages privés de ponctuation
et écrits uniquement avec des lettres majuscules :

"UN TEL MESSAGE EST ACCEPTE"

De plus, on enlevera les espaces :

"UNTELMESSAGEESTACCEPTE"

# Le programme fourni

Il comporte différentes fonctions. Certaines sont incomplètes, d'autres
ne sont pas documentées.

* `lettres` reçoit une phrase en paramètre et retourne cette phrase,
    en majuscule et sans les espaces. Cette fonction n'est pas programmée du
    tout.

    Par exemple :

    ```python
    >>> lettres(" salut les jeunes ")
    "SALUTLESJEUNES"
    ```

* `decaler` prend deux paramètres `lettre`, une chaîne d'un caractère et
    `decalage`, un entier entre 0 et 25 (notre alphabet comporte 26 lettres).
    Elle renvoie la lettre obtenue par un décalage circulaire.

    ```python
    >>> decaler("A", 2)
    "C"
    >>> decaler("Y", 4)
    "C"
    ```

* `mesurer_decalage` reçoit une lettre majuscule (chaîne d'un caractère)
    et renvoie le nombre de caractère qui sépare cette lettre du "A"

    ```python
    >>> mesurer_decalage("A")
    0
    >>> mesurer_decalage("L")
    11
    ```

* `vigenere` réalise un chiffrement de Vigenère pour un message (`str`) et une
    clé (`str`). Un troisième paramètre est fourni selon qu'on souhaite
    chiffrer (on passe la valeur `"chiffrer"`) ou déchiffrer (on passe la valeur
    `"dechiffrer"`)

Rappel :

* `chr` prend un entier en paramètre et renvoie la lettre lui correspondant
    en UTF-8. D'autre part, UTF-8 et ASCII se correspondent pour les 127
    premiers caractères.
* `ord` est la réciproque de `chr`. Elle prend un caractère et renvoie
    sa position dans la table `utf-8`.

# Consignes

1. La fonction `decaler` n'est pas documentée. Ajouter sa documentation.
    On n'oubliera pas les types, soit dans la documentation soit à l'aide
    d'indication de types.
2. La fonction `lettres` n'est pas du tout programmée. Programmez la.
    On pourra utiliser la méthode "`upper`" des chaînes de caractères.

    ```python
    >>> chaine = "bonjour"
    >>> chaine.upper()
    'BONJOUR'
    ```
    On pourra utilier la méthode `replace` de la calsse `str` dont on pourra regarder l'aide.
    
3. La fonction `vigenere` est insatisfaisante. En effet, on ne vérifie pas
    que le troisième paramètre `mode` est bien `"chiffrer"` ou `"dechiffrer"`.
    On provoque une erreur difficile à comprendre si on transmet une autre
    valeur.

    Ajoutez une assertion au début de la fonction afin de provoquer une erreur
    si le troisième paramètre n'est ni chiffrer ni dechiffrer.
4. Toujours concernant la fonction `vigenere`, le code comporte deux parties
    très similaires. Proposer une amélioration afin d'éviter ce copier-coller.
