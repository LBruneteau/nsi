---
title: "NSI - Terminale - Épreuve pratique"
subtitle: "Sujet 3"
---

## Modalités

* Vous avez une heure pour accomplir ces deux exercices.
* Vous rendez les deux fichiers (`exo1_votre_nom.py` et `exo2_votre_nom.py`) en les déposant dans un dossier portant votre nom sur le bureau. Assurez-vous de rendre les bons fichiers.
* Vous pouvez solliciter l'examinateur, l'échange oral avec l'examinateur fait partie de l'évaluation. 


# Exercice 1 - Restitution d'un algorithme

## Algorithme glouton : rendu de monnaie

L'objectif de cet exercice est de créer une fonction `monnaie` qui reçoit deux
paramètres (`somme`, un entier et `pieces`, une liste Python) et qui renvoie une
liste Python des pièces (de la liste `pieces`) à rendre pour atteindre `somme`.

On supposera :

1. Que les pièces fournies sont triées par ordre décroissant (Exemple : `[5, 2, 1]`),
2. Que toutes les pièces ont des valeurs supérieures à zéro,
3. Que les pièces sont toutes différentes (`pieces = [2, 2, 1]` n'est pas valide).
3. Que `somme` est supérieur ou égal à zéro.

Une pièce peut être utilisée plusieurs fois.

Par exemple :

```python
>>> pieces = [5, 2, 1]
>>> somme = 13
>>> monnaie(somme, pieces)
[5, 5, 2, 1]
```

Qui s'interprète ainsi : "_pour rendre 13€ avec les pièces 5€, 2€ et 1€ il faut rendre 5€, 5€, 2€ et 1€_".

Le code fourni contient une fonction de test permettant de vérifier vos travaux.

Consignes : **Écrire une fonction `monnaie` qui prend deux paramètres :**

* `pieces` : une liste de pièces vérifiant les préconditions énoncées plus haut,
* `somme` : un entier positif ou nul,

**Votre fonction renvoie une liste Python des pièces à rendre pour atteindre somme, par ordre décroissant.**




