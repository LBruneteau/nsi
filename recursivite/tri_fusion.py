from typing import Callable, TypeVar, List

T = TypeVar('T')

def fusion(a: List[T], b: List[T]) -> List[T]:
    if not a:
        return b
    if not b:
        return a
    if a[0] < b[0]:
        return a[:1] + fusion(a[1:], b)
    return b[:1] + fusion(a, b[1:])
    
def tri_fusion(xs: List[T]) -> List[T]:
    l: int = len(xs)
    if l <= 1:
        return xs
    return fusion(tri_fusion(xs[:l//2]), tri_fusion(xs[l//2:]))

