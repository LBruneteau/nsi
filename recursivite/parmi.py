def parmi(a: int, b: int) -> int:
    '''Retourne le nombre de chemins de b parmi a
    '''
    if b in {0, a}:
        return 1
    return parmi(a-1, b-1) + parmi(a-1, b)