import random
from typing import List, Tuple, TypeVar

T = TypeVar('T')

def partition(xs: List[T], elem: int) -> Tuple[List[T], List[T]]:
    x1: List[T]
    x2: List[T]
    x1, x2 = [], []
    for i, x in enumerate(xs):
        if i == elem:
            continue
        if x <= xs[elem]:
            x1.append(x)
        else:
            x2.append(x)
    return x1, x2

def rapide(xs: List[T]) -> List[T]:
    if len(xs) <= 1:
        return xs
    pivot: T = random.randrange(len(xs))
    x1, x2 = partition(xs, pivot)
    return rapide(x1) + xs[pivot:pivot+1] + rapide(x2)