from typing import Any, List, Optional, Generic, Tuple, TypeVar

T = TypeVar('T')
U = TypeVar('U')

class ArbreBin(Generic[T, U]):

    '''Arbre binaire
    '''

    def __init__(self, valeur: T, e1: Optional[U]=None, e2: Optional[U]=None) -> None:
        self.valeur: T = valeur
        self.arbre_gauche: Optional['ArbreBin[U, Any]'] = ArbreBin(e1) if e1 is not None else None
        self.arbre_droite: Optional['ArbreBin[U, Any]'] = ArbreBin(e2) if e2 is not None else None

    @property
    def gauche(self) -> Optional[U]:
        return None if self.arbre_gauche is None else self.arbre_gauche.valeur

    @gauche.setter
    def gauche(self, val: U) -> None:
        if self.arbre_gauche is None:
            self.arbre_gauche = ArbreBin(val)
        else:
            self.arbre_gauche.valeur = val

    @property
    def droite(self) -> Optional[U]:
        return None if self.arbre_droite is None else self.arbre_droite.valeur

    @droite.setter
    def droite(self, val: U) -> None:
        if self.arbre_droite is None:
            self.arbre_droite = ArbreBin(val)
        else:
            self.arbre_droite.valeur = val

    @property
    def enfants(self) -> Tuple[Optional[U], Optional[U]]:
        return self.gauche, self.droite

    @property
    def feuille(self) -> bool:
        return self.arbre_gauche is None and self.arbre_droite is None

    @property
    def hauteur(self) -> int:
        if self.feuille:
            return 0
        return max(self.arbre_gauche.hauteur, self.droite.hauteur) + 1