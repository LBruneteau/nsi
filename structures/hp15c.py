import operator
import math
import tkinter
from typing import Callable
from piles import Pile

OPER: dict[str, Callable[..., float]] = {
    '+' : operator.add,
    '-' : operator.sub,
    '*' : operator.mul,
    '/' : operator.truediv,
    '%' : operator.mod,
    '//' : operator.floordiv,
    '**' : operator.pow,
    'pi' : lambda : math.pi,
    '++' : lambda x : x + 1,
    '--' : lambda x : x - 1,
    'neg' : lambda x : x if x > 0 else -x,
    'abs' : abs,
    'flr' : math.floor,
    'cos' : math.cos,
    'min' : min,
    'sin' : math.sin,
    'acos' : math.acos,
    'sqr' : lambda x : x ** 2,
    'asin' : math.asin,
    'sqrt' : math.sqrt,
    'exp' : math.exp,
    'max' : max,
    'atg' : math.atan,
    'rnd' : round
}

ARGS: dict[str, int] = {
    '+' : 2,
    '-' : 2,
    '*' : 2,
    '/' : 2,
    '%' : 2,
    '//' : 2,
    '**' : 2,
    'pi' : 0,
    '++' : 1,
    '--' : 1,
    'neg' : 1,
    'abs' : 1,
    'flr' : 1,
    'cos' : 1,
    'min' : 2,
    'sin' : 1,
    'acos' : 1,
    'sqr' : 1,
    'asin' : 1,
    'sqrt' : 1,
    'exp' : 1,
    'max' : 2,
    'atg' : 1,
    'rnd' : 1
}

def npi(formule: str) -> float:
    '''Retourne le résultat de la formule npi en str

    Args:
        formule (str): la notation polonaise

    Returns:
        float: Calcul effectué
    '''
    pile: Pile[float]= Pile()
    liste: list[str] = formule.split()
    for elem in liste:
        try:
            elem = float(elem)
        except ValueError:
            func = OPER[elem]
            nb_args = ARGS[elem]
            args = reversed([pile.pop() for _ in range(nb_args)])
            pile.push(func(*args))
        else:
            pile.push(elem)
    res = pile.pop()
    return int(res) if res == int(res) else res

def gui():
    root = tkinter.Tk()
    label = tkinter.Label(root)
    entry = tkinter.Entry(root)
    btn = tkinter.Button(root, text='Calculer', command=lambda: label.config(text=npi(entry.get())))
    entry.pack()
    label.pack()
    btn.pack()
    root.mainloop()
    
if __name__ == '__main__':
    gui()