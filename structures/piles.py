from typing import Generic, TypeVar, Optional
from maillon import Maillon

T = TypeVar('T')

class Pile(Generic[T]):

    def __init__(self, *args: T) -> None:
        '''Création d'une pile, vide par défaut
        args: Les éléments à ajouter dans la pile. 
        Le dernier élément fourni sera le sommet de la pile.
        '''
        self._sommet: Optional[Maillon[T]] = None
        for i in reversed(args):
            self.push(i)

    def __repr__(self) -> str:
        return '[Vide]' if self.vide else f'[{self._sommet}]'

    @property
    def sommet(self) -> T:
        '''Retourne la valeur du sommet de la pile
        '''
        return self._sommet.val

    @sommet.setter
    def sommet(self, valeur: T) -> None:
        '''
        '''
        maillon: Maillon[T] = Maillon(valeur)
        maillon.suiv = self._sommet.suiv
        self._sommet = maillon
    
    @property
    def vide(self) -> bool:
        return self._sommet is None

    def push(self, valeur: T) -> None:
        '''Ajoute un élément au sommet de la pile
        Args:
            valeur (T) -> La valeur à ajouter à la pile
        '''
        maillon: Maillon[T] = Maillon(valeur)
        maillon.suiv = self._sommet
        self._sommet = maillon

    def pop(self) -> Optional[T]:
        '''Retire le sommet de la pile et le retourne.
        Si la pile est vide alors None est retourné et la pile reste inchangée
        '''
        if self.vide:
            return None
        sommet: Optional[Maillon[T]] = self._sommet
        self._sommet = sommet.suiv
        return sommet.val

    def ecrase(self) -> None:
        '''Vide completement la pile
        Complexité O(n)
        '''
        while not self.vide:
            self.pop()

    def inverse(self) -> None:
        '''Inverse l'ordre des éléments dans la pile
        Complexité O(4n)
        '''
        copie: Pile[T] = self.copie()
        self.ecrase()
        while not copie.vide:
            self.push(copie.pop())

    def copie(self) -> 'Pile[T]':
        '''Retourne une copie de la pile
        Complexité O(3n)
        '''
        temp: 'Pile[T]' = Pile()
        resultat: 'Pile[T]' = Pile()
        while not self.vide:
            temp.push(self.pop())
        while not temp.vide:
            elem: T = temp.pop()
            self.push(elem)
            resultat.push(elem)
        return resultat

    def duplique(self) -> None:
        '''Duplique l'élément au sommet de la pile
        '''
        maillon = Maillon(self.sommet)
        maillon.suiv = self._sommet
        self._sommet = maillon

    def echange(self) -> None:
        '''échange les valeurs des deux premiers éléments de la liste
        '''
        m1: Maillon[T] = Maillon(self._sommet.suiv.val)
        m2: Maillon[T] = Maillon(self.sommet)
        m1.suiv = m2
        m2.suiv = self._sommet.suiv.suiv
        self._sommet = m1