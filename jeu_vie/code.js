let animationRunning = false;
let canvas = document.querySelector('canvas');
let context = canvas.getContext('2d');
let btn = document.getElementById('btn');
let emptyBtn = document.getElementById('empty-button');
let randomBtn = document.getElementById('random-button');

function drawGrid(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    for(let x = 0; x <= canvas.width; x += 10){
        context.beginPath();
        context.moveTo(x, 0);
        context.lineTo(x, canvas.height);
        context.stroke();
    }
    for(let y = 0; y <= canvas.width; y += 10){
        context.beginPath();
        context.moveTo(0, y);
        context.lineTo(canvas.width, y);
        context.stroke();
    }
}

class Cell{

    static width = 10;
    static height = 10;

    constructor(x, y, alive=Math.random()<0.3){
        this.x = x
        this.y = y
        this.alive = alive
        this.nextAlive = false;
    }

    draw(){
        if (this.alive){
            context.fillRect(this.x * Cell.width, this.y * Cell.height, Cell.width, Cell.height);
        }
    }
}

class Game{

    constructor(columns, rows, random=true){
        this.columns = columns;
        this.rows = rows;
        this.grid = [];
        this.createGrid(random);

    }

    createGrid(random){
        this.grid = [];
        for (let y = 0; y < this.rows; y++){
            let row = [];
            for (let x = 0; x < this.columns; x++){
                let alive = random ? Math.random() < 0.3 : false;
                let cell = new Cell(x, y, alive);
                row.push(cell);
            }
            this.grid.push(row);
        }
    }

    aliveNeighbours(x, y){
        let nb = 0;
        for (let i = y - 1; i < y + 2; i++){
            for (let j = x - 1; j < x + 2; j++){
                if (i >= 0 && i < this.rows && j >= 0 && j < this.columns && !(x === j && y == i) && this.grid[i][j].alive){
                    nb ++;
                }
            } 
        }
        return nb;
    }

    nextGen(){
        for (let y = 0; y < this.rows; y++){
            for (let x = 0; x < this.columns; x++){
                let nb = this.aliveNeighbours(x, y);
                this.grid[y][x].nextAlive = (nb === 3) || (this.grid[y][x].alive && nb === 2);
            }
        }
        for (let y = 0; y < this.rows; y++){
            for (let x = 0; x < this.columns; x++){
                this.grid[y][x].alive = this.grid[y][x].nextAlive
            }
        }
    }

    draw(){
        drawGrid()
        for (let y = 0; y < this.rows; y++){
            for (let x = 0; x < this.columns; x++){
                let cell = this.grid[y][x];
                cell.draw();
            }
        }
    }

    gameLoop(){
        this.nextGen();
        context.clearRect(0, 0, canvas.width, canvas.height);
        this.draw();
        if (animationRunning){
            setTimeout( () => {window.requestAnimationFrame( () => this.gameLoop())}, 100);
        }
    }
}

let game = new Game(80, 80, false);
drawGrid();

function clickOnCanvas(event){
    let rect = canvas.getBoundingClientRect();
    let x = Math.floor((event.clientX - rect.left) / 10);
    let y = Math.floor((event.clientY - rect.top) / 10);
    if (!animationRunning){
        game.grid[y][x].alive = !game.grid[y][x].alive;
        game.draw()
    }
}

function buttonClick(){
    animationRunning = !animationRunning
    if (animationRunning){
        btn.innerHTML = 'Stopper l\'animation';
        randomBtn.style.visibility = 'hidden';
        emptyBtn.style.visibility = 'hidden';
        window.requestAnimationFrame(() => {game.gameLoop();});
    }
    else{
        btn.innerHTML = 'Lancer l\'animation';
        randomBtn.style.visibility = 'visible';
        emptyBtn.style.visibility = 'visible';
    }
}

btn.addEventListener('click', buttonClick);
emptyBtn.addEventListener('click', () => {game = new Game(80, 80, false); drawGrid();});
randomBtn.addEventListener('click', () => {game = new Game(80, 80); game.draw()});
canvas.addEventListener('mousedown', clickOnCanvas);